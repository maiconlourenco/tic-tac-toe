let currentPlayer = 'X'
let nextPlayer = 'O'

let playerXselections = []
let playerOselections = []
let selected = []

let playerXwins = 0
let playerOwins = 0
let draws = 0

let text = document.getElementById('texto').textContent = `Vez do Jogador: ${currentPlayer}`
let winsCount = document.getElementById('vitorias').textContent = `Vitórias: JogadorX [ ${playerXwins} ] / JogadorO [ ${playerOwins} ]`
let Empates = document.getElementById('empates').textContent = `Empates: ${draws}`

const winComb = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7]
]

function handleClick(event) {
    const cell = event.target

    console.log(selected)
    if (selected.indexOf(Number(cell.id)) !== -1) {
        alert('invalid')
    } else {

        cell.innerHTML = currentPlayer

        if (currentPlayer === 'X' ) {
            playerSelections = playerXselections
            nextPlayer = 'O'
        } else {
            playerSelections = playerOselections
            nextPlayer = 'X'
        }

        playerSelections.push(Number(cell.id))
        selected.push(Number(cell.id))
    
        if (checkWinner(playerSelections)) {
            alert('Player ' + currentPlayer + ' Wins!!!')
            if (currentPlayer === 'X' ) {
                playerXwins++
            } else {
                playerOwins++
            }
            resetGame()
        }

        if (checkDraw()) {
            alert('Draw!!!')
            draws++
            resetGame()
        }
        let text = document.getElementById('texto').textContent = `Vez do Jogador: ${nextPlayer}`
        let winsCount = document.getElementById('vitorias').textContent = `Vitórias: Jogador X [ ${playerXwins} ] / Jogador O [ ${playerOwins} ]`
        let Empates = document.getElementById('empates').textContent = `Empates: ${draws}`
        currentPlayer = nextPlayer 
    }
    
}

const cells = document.querySelectorAll('td')

for (let i = 0; i < cells.length;i++ ) {
    cells[i].addEventListener('click', handleClick)
}


function checkWinner(player) {
    for (let indx = 0; indx < winComb.length; indx++) {
        let win = winComb[indx]
        matches = 0
        for (let x of win) {
            if (player.includes(x)) {
                matches++
            } else {
                break
            }
            if (matches === 3) {
                return true
            }
        }
    }
    return false
}

function checkDraw() {
    return (playerOselections.length + playerXselections.length) >=cells.length
}

function resetGame() {
    playerXselections = new Array()
    playerOselections = new Array()
    selected = new Array()
    for (let i = 0; i < cells.length; i++) {
        cells[i].innerHTML = ''
    }
}